 create table t_case 
 (
	case_order int, 
	uuid text primary key not null, 
	case_name text,
	file_name text,
	login_company_code text,
	error_msg text,
	error_img_name text,
	infos text,
	last_run_time int,
	use_times real,
	status text
  )
