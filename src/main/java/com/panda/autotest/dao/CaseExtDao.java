package com.panda.autotest.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.panda.autotest.dao.model.Case;

@Repository
public interface CaseExtDao {
	void deleteALL();

	List<Case> selectALL();

	int updateCaseInfo(Case record);
}