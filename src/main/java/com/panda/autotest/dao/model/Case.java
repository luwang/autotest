package com.panda.autotest.dao.model;

import java.io.Serializable;

/**
 * @author chen.pian
 *
 * @date 2017-12-25 17:00:58
 *
 */
public class Case implements Serializable {
    private String uuid;

    private Long caseOrder;

    private String caseName;

    private String fileName;

    private String loginCompanyCode;

    private String errorMsg;

    private String errorImgName;

    private String infos;

    private Long lastRunTime;

    private Double useTimes;

    private String status;

    private static final long serialVersionUID = 1L;

    /**
     * @return uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return case_order
     */
    public Long getCaseOrder() {
        return caseOrder;
    }

    /**
     * @param caseOrder
     */
    public void setCaseOrder(Long caseOrder) {
        this.caseOrder = caseOrder;
    }

    /**
     * @return case_name
     */
    public String getCaseName() {
        return caseName;
    }

    /**
     * @param caseName
     */
    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    /**
     * @return file_name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return login_company_code
     */
    public String getLoginCompanyCode() {
        return loginCompanyCode;
    }

    /**
     * @param loginCompanyCode
     */
    public void setLoginCompanyCode(String loginCompanyCode) {
        this.loginCompanyCode = loginCompanyCode;
    }

    /**
     * @return error_msg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return error_img_name
     */
    public String getErrorImgName() {
        return errorImgName;
    }

    /**
     * @param errorImgName
     */
    public void setErrorImgName(String errorImgName) {
        this.errorImgName = errorImgName;
    }

    /**
     * @return infos
     */
    public String getInfos() {
        return infos;
    }

    /**
     * @param infos
     */
    public void setInfos(String infos) {
        this.infos = infos;
    }

    /**
     * @return last_run_time
     */
    public Long getLastRunTime() {
        return lastRunTime;
    }

    /**
     * @param lastRunTime
     */
    public void setLastRunTime(Long lastRunTime) {
        this.lastRunTime = lastRunTime;
    }

    /**
     * @return use_times
     */
    public Double getUseTimes() {
        return useTimes;
    }

    /**
     * @param useTimes
     */
    public void setUseTimes(Double useTimes) {
        this.useTimes = useTimes;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", caseOrder=").append(caseOrder);
        sb.append(", caseName=").append(caseName);
        sb.append(", fileName=").append(fileName);
        sb.append(", loginCompanyCode=").append(loginCompanyCode);
        sb.append(", errorMsg=").append(errorMsg);
        sb.append(", errorImgName=").append(errorImgName);
        sb.append(", infos=").append(infos);
        sb.append(", lastRunTime=").append(lastRunTime);
        sb.append(", useTimes=").append(useTimes);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}