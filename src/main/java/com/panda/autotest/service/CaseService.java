package com.panda.autotest.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.panda.autotest.controller.CaseSearchCriteria;

public interface CaseService {
	CaseSearchCriteria getCaseList(CaseSearchCriteria searchCriteria);

	void createCase(CommonsMultipartFile file);

	void deleteCases(List<String> keys);
	
	void updateCase(Map<String, String> dataMap);
	
	void refreshCases(List<String> keys);
}
