package com.panda.autotest.core.parse;

public enum Type {
	/**
	 * 点击
	 */
	click("点击"), input("输入"), select("选择"), date("输入"), radio("选择"), url("定位"), changeEodDate("更新EodDate"), login("登录"), submit(
			"提交"), job("定时任务"), queryWaitsql("等待执行"), updateSql("执行更新Sql"), validate("验证"), validateEod(
			"验证EodDate是否切换成功");

	private String name;

	private Type(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
