package com.panda.autotest.core;

public class AutoTestConstants {

	// case 运行状态
	public static final String SUCCESS = "1";
	public static final String RUN = "2";
	public static final String FAIL = "3";
	public static final String WAIT_RUN = "4";
}
