package com.panda.autotest.core.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource {
	private static final ThreadLocal<String> contextHolder = new ThreadLocal<>();

	@Override
	protected Object determineCurrentLookupKey() {
		return getDataSource();
	}

	public static void setDataSource(String schema) {
		contextHolder.set(schema + "DataSource");
	}

	public static String getDataSource() {
		String dataSource = contextHolder.get();
		if (null == dataSource) {
			contextHolder.set("cmsDataSource");
		}
		return contextHolder.get();
	}

	public static void clearDataSource() {
		contextHolder.remove();
	}
	
}
