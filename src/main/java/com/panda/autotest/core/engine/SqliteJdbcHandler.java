package com.panda.autotest.core.engine;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

public class SqliteJdbcHandler extends JdbcDaoSupport {
	private Logger logger = LoggerFactory.getLogger(SqliteJdbcHandler.class);

	/**
	 * @author chen.pian
	 * @param sql
	 */
	public void initSql(String sql) {
		logger.info(sql);
		this.getJdbcTemplate().update(sql);
	}

	/**
	 * @author chen.pian
	 * @param sql
	 * @return
	 */
	public Map<String, Object> queryForMap(String sql) {
		logger.info(sql);
		return this.getJdbcTemplate().queryForMap(sql);
	}

	@Transactional
	public synchronized long nextSequence() {
		List<Map<String, Object>> resultList = this.getJdbcTemplate().queryForList("select num from t_sequence");
		long num = 1;
		if (resultList != null && resultList.size() > 0) {
			num = (int) resultList.get(0).get("num");
			num++;
			this.getJdbcTemplate().update("update t_sequence set num =" + num);
		} else {
			this.getJdbcTemplate().update("insert into t_sequence(num) values(" + num + ")");
		}
		return num;
	}
}
