package com.panda.autotest.core.engine;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.panda.autotest.core.parse.CaseConfig;

public class CaseRunnable implements Runnable {
	private Logger logger = LoggerFactory.getLogger(AutoTestEngine.class);
	private CaseHandler caseHandler;
	private List<CaseConfig> caseConfigList;
	private CountDownLatch countDownLatch;

	public CaseRunnable(CountDownLatch countDownLatch, CaseHandler caseHandler, List<CaseConfig> caseConfigList) {
		this.caseHandler = caseHandler;
		this.caseConfigList = caseConfigList;
		this.countDownLatch = countDownLatch;
	}

	@Override
	public void run() {
		try {
			for (CaseConfig caseConfig : caseConfigList) {
				try {
					caseHandler.handle(caseConfig);
				} catch (Throwable e) {
					logger.error("error", e);
				}
			}
		} finally {
			countDownLatch.countDown();
		}
	}

}
