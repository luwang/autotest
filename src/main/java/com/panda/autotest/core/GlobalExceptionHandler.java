package com.panda.autotest.core;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.panda.autotest.core.ajax.Result;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Result handleSQLException(HttpServletRequest request, Exception e) {
		return new Result(e.getMessage());
	}

}