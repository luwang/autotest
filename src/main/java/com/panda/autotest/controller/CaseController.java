package com.panda.autotest.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.panda.autotest.core.AutoTestConstants;
import com.panda.autotest.core.ajax.Result;
import com.panda.autotest.core.engine.AutoTestEngine;
import com.panda.autotest.core.parse.CaseConfig;
import com.panda.autotest.core.parse.CaseConfigManager;
import com.panda.autotest.service.CaseService;

@Controller
@RequestMapping("case")
public class CaseController {
	@Autowired
	private CaseConfigManager caseConfigManager;
	@Autowired
	private AutoTestEngine autoTestEngine;
	@Value("${case.img.dir}")
	private String caseImgDir;

	@Autowired
	private CaseService caseService;

	@RequestMapping("/list")
	@ResponseBody
	public Result caseList(@RequestBody CaseSearchCriteria searchCriteria) {
		return new Result().with(caseService.getCaseList(searchCriteria));
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody List<String> keys) {
		if (keys == null || keys.size() <= 0) {
			return new Result();
		}
		caseService.deleteCases(keys);
		return new Result();
	}

	@RequestMapping("/run")
	@ResponseBody
	public Result run(@RequestBody List<String> keys) {
		if (keys == null || keys.size() <= 0) {
			return new Result();
		}
		autoTestEngine.start(keys);
		return new Result();
	}

	@RequestMapping("/query")
	@ResponseBody
	public Result query(@RequestBody Map<String, String> dataMap) throws IOException {
		CaseConfig caseConfig = caseConfigManager.getCaseConfigMap().get(dataMap.get("uuid"));
		InputStream inputStream = null;
		String xml = null;
		try {
			inputStream = new FileInputStream(new File(caseConfig.getFilePath()));
			xml = IOUtils.toString(inputStream, "UTF-8");
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return new Result().with(xml);
	}

	@RequestMapping("/queryRunInfo")
	@ResponseBody
	public Result queryRunInfo(@RequestBody List<String> keys) throws IOException {
		if (keys == null || keys.size() <= 0) {
			return new Result();
		}
		List<CaseConfig> caseConfigList = new ArrayList<CaseConfig>();
		for (String key : keys) {
			caseConfigList.add(caseConfigManager.getCaseConfigMap().get(key));
		}
		return new Result().with(caseConfigList);
	}

	@RequestMapping("/refresh")
	@ResponseBody
	public Result refresh(@RequestBody List<String> keys) throws Exception {
		if (keys == null || keys.size() <= 0) {
			return new Result();
		}
		caseService.refreshCases(keys);
		return new Result();
	}

	@RequestMapping("/stop")
	@ResponseBody
	public Result stop(@RequestBody List<String> keys) throws Exception {
		if (keys == null || keys.size() <= 0) {
			return new Result();
		}
		for (String key : keys) {
			CaseConfig caseConfig = caseConfigManager.getCaseConfigMap().get(key);
			if (caseConfig == null) {
				throw new RuntimeException("案例已经被删除或更新");
			}
			if (AutoTestConstants.SUCCESS.equals(caseConfig.getStatus())
					|| AutoTestConstants.FAIL.equals(caseConfig.getStatus())) {
				throw new RuntimeException("案例没有运行");
			}
			caseConfig.setForceStop(true);
		}
		return new Result();
	}

	@RequestMapping("/update")
	@ResponseBody
	public Result update(@RequestBody Map<String, String> dataMap) throws Exception {
		caseService.updateCase(dataMap);
		return new Result();
	}

	@RequestMapping("/errorImg")
	public void errorImg(@RequestParam("imageFileName") String imageFileName, HttpServletResponse response)
			throws Exception {
		if (imageFileName != null) {
			File desFile = new File(caseImgDir + File.separator + imageFileName);
			IOUtils.copy(new FileInputStream(desFile), response.getOutputStream());
			response.flushBuffer();
			response.getOutputStream().close();
		} else {
			throw new RuntimeException("案例已经被删除或更新");
		}
	}

}
