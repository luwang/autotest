package com.panda.autotest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.panda.autotest.core.ajax.Result;
import com.panda.autotest.service.CaseService;

@Controller
@RequestMapping("file")
public class FileController {
	@Autowired
	private CaseService caseService;

	@RequestMapping("/upload")
	@ResponseBody
	public Result upload(@RequestParam(value = "file") CommonsMultipartFile file) throws Exception {
		caseService.createCase(file);
		return new Result();
	}
}
