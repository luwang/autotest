'use strict';
(function(angular) {
  var locales = {};
  angular.module("app").constant('$locale', locales);
  locales.id = "zh_CN";
  locales.DATETIME_FORMATS = {
    "TITLE": ["年", "月", "日"],
    "MONTH": ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    "SHORTMONTH": ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    "DAY": ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
    "SHORTDAY": ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
    "dateTime": "yyyy-MM-dd HH:mm:ss",
    "mediumDate": "yyyy-MM-dd",
  };

  locales.NUM_FORMATS = {
    "integer": "#0",
    "interestRate": "#0.0######",
    "money0": "#,##0",
    "decimalSeparator": ".",
    "float": "#0.0##############",
    "money1": "#,##0.0",
    "money2": "#,##0.00",
    "money3": "#,##0.000",
    "groupSeparator": ",",
    "fxRate": "#0.0###########",
    "money": "#,##0.00#"
  };
  var messages = {};
  locales.MESSAGES = messages;

  messages["menu.test"] = '测试管理';
  messages["menu.case.list"] = '案例';
  messages["menu.monitor.list"] = '监控中心';
  messages.icon = {};
  messages.icon["menu.test"] = 'icon-settings';

  messages["STATUS"] = {
    "1": "正常",
    "2": "运行",
    "3": "错误",
    "4": "等待运行"
  }

})(window.angular);