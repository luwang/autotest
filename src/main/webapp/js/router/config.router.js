'use strict';

/**
 * 配置路由
 */
angular.module('app').run(['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
}]).config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/app/home');
  $stateProvider.state('app', {
    abstract: true,
    url: '/app',
    templateUrl: 'tpl/app.html'
  }).state('app.home', {
    url: '/home',
    templateUrl: 'tpl/dashboard.html'
  }).state('app.sys', {
    url: '/sys',
    template: '<div class="app-content-body fade-in-up" ui-view></div>'
  }).state('app.sys.case', {
    url: '/case/list',
    templateUrl: 'views/case/list.html',
    controller: "caseCtrl",
    resolve: {
      deps: ['$ocLazyLoad', function($ocLazyLoad) {
        return $ocLazyLoad.load('views/case/case.js');
      }]
    }
  }).state('access', {
    url: '/access',
    template: '<div ui-view class="fade-in-right-big smooth"></div>'
  }).state('access.404', {
    url: '/404',
    templateUrl: 'tpl/page_404.html'
  }).state('access.500', {
    url: '/500',
    templateUrl: 'tpl/page_500.html'
  }).state('access.401', {
    url: '/401',
    templateUrl: 'tpl/page_401.html'
  })
}]);