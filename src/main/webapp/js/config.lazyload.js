angular.module('app').constant(
		"CommonConstant",{
			USER_INFO : "USER_INFO",
			MENU_INFO: "MENU_INFO"
		}
	).config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
	  $ocLazyLoadProvider.config({
	    debug: false,
	    events: true,
	    disableCache: true, //default false
	    modules: [{
          	      name: 'toaster',
          	      files: ['js/modules/toaster/toaster.js', 'js/modules/toaster/toaster.css']
                 }
             ]
	  });
}]);
