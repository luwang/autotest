'use strict';
/**
 * 系统中所有的自定义过滤器在此处理，每个过滤器的名称及用途都列在此处
 * fnumber:数字格式化：动态获取数字格式化格式
 * 用法：{{"98222.1" | fnumber:'money3'}}-->98,222.100
 * 
 * fmoney:币种格式化处理
 * 用法：{{"98222.1" | fmoney:'CNY'}}
 * 
 * fpercentage:将小数格式化成百分比，并加上百分号
 * 用法： {{"0.95" | fpercentage: 2}} --> 95.00% 或者 {{"0.95" | fpercentage: 'interestRate'}}
 * 默认值: numDecimal: 2 
 */
angular.module('app') 
.filter('fnumber', ['$filter','$locale','decimalService',function($filter,$locale,decimalService) {
    return function(value,type) {
    	if(angular.isUndefined(value)&&!angular.isNumber(value)){
    		return "";
    	}
    	return decimalService.format(value,type);
    }
}]);


angular.module('app') 
.filter('fmoney', ['$filter','$locale','decimalService',function($filter,$locale,decimalService) {
    return function(value,ccy) {
    	if(angular.isUndefined(value)&&!angular.isNumber(value)){
    		value = 0;
    	}
    	if(angular.isUndefined($locale.CURRENCY[ccy])){
    		$locale.CURRENCY = angular.fromJson(sessionStorage.getItem("CURRENCY"));
    	}
    	var ccyObj = $locale.CURRENCY[ccy];
    	if(ccyObj==null){
    		return value;
    	}
    	var numDecimal = ccyObj.decimalnum;
    	if(!angular.isNumber(value)){
    		value=decimalService.parse(value);
    	}
    	ccy='money'+numDecimal;
    	return decimalService.format(value,ccy);
    }
}]);

	
angular.module('app') 
.filter('fpercentage', ['$filter','$locale','decimalService','arithService', 
    function($filter,$locale,decimalService, arithService) {
    return function(value,numDecimal) {
    	if(angular.isUndefined(value) && !angular.isNumber(value)){
    		return "";
    	}
    	value = decimalService.parse(value);
    	var pctValue = arithService.mul(value, 100);
    	if (!angular.isUndefined(numDecimal)&&!angular.isNumber(numDecimal)) {
    		var decimaltype = numDecimal;
         	//当指定格式模板时用模板进行格式化
         	return decimalService.format(pctValue, decimaltype) + '%';
    	} else {
    		numDecimal = numDecimal || 2;
    	}
    	return $filter('number')(pctValue, numDecimal) + '%';
    }
}]);