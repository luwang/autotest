'use strict';
app.service("commonService", commonService);
commonService.$injector = ['$http', '$q', '$state', '$rootScope'];
function commonService($http, $q, $state, $rootScope) {
  /** 请求后台接口* */
  this.post = function(actionId, formVo) {
    if (!formVo) {
      var formVo = {};
    }
    var deferred = $q.defer();
    $http({
      method: 'post',
      url: actionId,
      data: formVo
    }).then(function successCallback(response) {
      return deferred.resolve(response.data);
    }, function errorCallback(response) {
      return deferred.reject(response.data);
    });
    return deferred.promise;
  }
}
