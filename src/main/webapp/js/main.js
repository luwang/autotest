'use strict';
angular.module('app').controller('AppCtrl', AppCtrl).run(function($rootScope, $locale, $state) {
  // 404监听事件
  $rootScope.$on('page:notFound', function(event) {
    $state.go('access.404');
  });
  $rootScope.$on('server:error', function(event) {
    $state.go('access.500');
  });
  $rootScope.$on('page:notAuthorized', function(event) {
    $state.go('access.401');
  });
});
AppCtrl.$injector = ['$scope', '$rootScope', '$translate', '$localStorage', '$window', 'commonService', '$ocLazyLoad',
    'CommonConstant', '$injector', '$state']
function AppCtrl($scope, $rootScope, $translate, $localStorage, $window, commonService, $ocLazyLoad, CommonConstant,
        $injector, $state) {
  // add 'ie' classes to html
  var isIE = !!navigator.userAgent.match(/MSIE/i);
  isIE && angular.element($window.document.body).addClass('ie');
  isSmartDevice($window) && angular.element($window.document.body).addClass('smart');

  $scope.app = {
    name: 'Angulr',
    version: '2.0.0',
    settings: {
      "themeID": "8",
      "navbarHeaderColor": "bg-info dker",
      "navbarCollapseColor": "bg-info dker",
      "asideColor": "bg-light dker b-r",
      "headerFixed": false,
      "asideFixed": false,
      "asideFolded": false,
      "asideDock": false,
      "container": false
    }
  }

  function isSmartDevice($window) {
    var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
    return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
  }
  $scope.langKey = "zh_cn";
  $ocLazyLoad.load(['js/l10n/locale_' + $scope.langKey + '.js']).then(function() {
    $rootScope.$msg = function(name) {
      return $injector.get("$locale").MESSAGES[name] || name;
    };
  })
 
  // 加载菜单
   $scope.menuItemList=[{
     "name": "menu.test",
     "action": null,
     "menuList": [{
         "name": "menu.case.list",
         "action": "app.sys.case",
         "menuList": null
       }
     ]
   }]
  // 跳转路由
  $scope.goto=function(url , param){
    if(url != '' && url != null){
        $state.go(url);
    }
  }
}